
// https://www.hackerrank.com/challenges/java-strings-introduction

package java_strings;

import java.io.*;
import java.util.*;

public class StringsIntroduction {


    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String a = sc.next();
        String b = sc.next();

        int length = a.length() + b.length();
        System.out.println(length);

        if(a.compareTo(b)>0)
            System.out.println("Yes");
        else
            System.out.println("No");

        String newA= a.substring(0,1).toUpperCase()+a.substring(1);
        String newB= b.substring(0,1).toUpperCase()+b.substring(1);
        System.out.println(newA + " " + newB);
    }
}




