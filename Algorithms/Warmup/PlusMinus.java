// https://www.hackerrank.com/challenges/plus-minus

import java.io.*;
import java.util.*;

class Result {

    /*
     * Complete the 'plusMinus' function below.
     *
     * The function accepts INTEGER_ARRAY arr as parameter.
     */

    public static void plusMinus(List<Integer> arr) {
        // Write your code here
        float pos = 0;
        float neg = 0;
        float nZeri = 0;

        int size = arr.size();

        for (int i = 0; i < size; i++) {
            if (arr.get(i) < 0) {
                neg += 1;
            } else if (arr.get(i) > 0) {
                pos += 1;
            } else {
                nZeri++;
            }

        }
        System.out.println(pos / size);
        System.out.println(neg / size);
        System.out.println(nZeri / size);

    }
}


    public class PlusMinus {
        public static void main(String[] args) throws IOException {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

            int n = Integer.parseInt(bufferedReader.readLine().trim());

            String[] arrTemp = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

            List<Integer> arr = new ArrayList<>();

            for (int i = 0; i < n; i++) {
                int arrItem = Integer.parseInt(arrTemp[i]);
                arr.add(arrItem);
            }

            Result.plusMinus(arr);

            bufferedReader.close();
        }

}
